# WSL2 우분투 20.04 AOSP 빌드 환경 구축

## 1. WSL2 리눅스 설치

WSL2 로 설치 가능한 우분투 버전:

- **Ubuntu 22.04**
- **Ubuntu 20.04**
- Ubuntu 18.04

```shell
# 특정 Linux 배포판 설치
$ wsl --install -d <Distribution Name>
$ wsl --install -d Ubuntu-22.04
```

### 1.1. 기타 WSL2 우분투 설정

<details>
<summary>보기 (선택 사항)</summary>

#### 1.1.1. zsh 설치

폰트 깨짐: fira code 설치

```shell
# 설치
$ sudo apt install zsh -y

# 버전 확인
$ zsh --version

# 기본 쉘 zsh로 변경
$ chsh -s /usr/bin/zsh

# zsh 설정 확인
# 우분투 터미널 재실행 후 2 선택
$ echo $SHELL
/usr/bin/zsh

# on-my-zsh 설치
$ sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# 테마 변경
$ cd
$ vi ~/.zshrc
# i를 누르고 편집이 끝나면 
# ESC 클릭 - :wq를 입력하시고 Enter로 저장 후 빠져나옵니다.
ZSH_THEME="agnoster"
```

</details>

### 1.2. WSL2 우분투 swap 설정

`C:\Users\<username>\.wslconfig`에 파일 생성후 아래와 같이 입력:
> 물리 메모리와 swap 총 합 40GB정도면 충분하다.

```text
[wsl2]
memory=8GB
swap=32GB
```

### 1.3. Certificate 설치

```zsh
# ca.der 파일을 ca.crt 로 변환
openssl x509 -inform DER -in ca.der -out ca.crt

# 디렉토리 생성
mkdir /usr/local/share/ca-certificates/

# ca.crt 이동
sudo cp ca.crt /usr/local/share/ca-certificates/

# 권한 부여
sudo chmod 644 /usr/local/share/ca-certificates/extra/*

# 이증서 등록
sudo dpkg-reconfigure ca-certificates
```

## 2. AOSP 빌드 환경 구축

### 2.1. 패키지 설치

```zsh
# 필요 패키지 설치
$ sudo apt install openjdk-8-jdk
$ sudo apt install git-core gnupg flex bison build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 libncurses5 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev libgl1-mesa-dev libxml2-utils xsltproc unzip fontconfig m4
```

### 2.2. 소스 제어 도구 (repo) 설치

```zsh
$ sudo apt install repo

# (위 방법 불가시) Repo 수동 설치
$ mkdir -p ~/.bin
$ PATH="${HOME}/.bin:${PATH}"
$ curl https://storage.googleapis.com/git-repo-downloads/repo > ~/.bin/repo
$ chmod a+rx ~/.bin/repo
# 출처: https://gerrit.googlesource.com/git-repo/#install
```

### 2.3. 소스 다운

#### 2.3.1. git 사용자 설정

```zsh
# git 사용자 변수
$ git config --global user.email <이메일 주소>
$ git config --global user.name <사용자 이름>
```

#### 2.3.2. (필요시) SSL 인증 비활성화

[1.3. Certificate 설치](#13-certificate-설치)를 했으면 건너뛰기.

```shell
$ git config --global http.sslVerify false
# repo 파일 수정
$ sudo nano ~/.bin/repo
# apt install repo 한 경우:
$ sudo nano /usr/bin/repo

# import sys 아래에 다음 추가:
import ssl
ssl._create_default_https_context = ssl._create_unverified_context
```

#### 2.3.3. 작업 폴더 생성 및 다운

```shell
# python 명령어에 python3 명령어 연결
$ sudo ln -s /usr/bin/python3 /usr/bin/python

# repo 설치 확인
$ repo version

# 작업 디렉토리 생성
$ mkdir ~/workspace/aosp
$ cd ~/workspace/aosp

# 소스 repo 다운
$ repo init -u https://android.googlesource.com/platform/manifest

# 마스터 분기 확인
$ repo init -u https://android.googlesource.com/platform/manifest -b android-10.0.0_r47
$ repo init -u https://android.googlesource.com/platform/manifest -b android-12.1.0_r27 

# 다운
$ repo sync -c -j8
```

#### 2.3.4. `mk_combined_img.py` 관련 에러 발생시

<details>
<summary>보기</summary>
python2 버전에서 python3 버전으로 변경

내장된 `mk_combined_img.py`는 python2 파일이지만, 우분투 20.04에서는 python2가 삭제됐으므로 python3 버전의 `mk_combined_img.py`로 대체해야한다.

파일: [`mk_combined_img.py`](https://android.googlesource.com/device/generic/goldfish/+/refs/heads/master/tools/mk_combined_img.py) 다운 후,

파일 위치: `device/generic/goldfish/tools/mk_combined_img.py` 으로 이동

</details>

### 2.4. AOSP 빌드

```shell
# Java Heap 사이즈 확장
$ export _JAVA_OPTIONS=-Xmx4g​

$ source build/envsetup.sh
$ lunch aosp_x86_64-eng
$ lunch aosp_car_x86_64-eng
$ m

```

#### 2.4.1. AOSP 빌드 결과

```shell
# 빌드 결과 이미지 확인
$ cd out/target/product/generic_x86_64
$ ls | grep .img
```

## 3. AOSP 이미지 실행

### ~~3.1. WSL2 우분투에서 에뮬레이터로 실행 (중첨된 가상화)~~

<details>
<summary>보기</summary>

- 윈도우 10: 실패 (중첩된 가상화 미지원)
- 윈도우 11: 성공

#### Qemu-kvm 패키지 설치

```shell
sudo apt install qemu-kvm
```

#### `kvm` 그룹에 사용자 추가

```shell
sudo usermod -a -G kvm <username>
```

#### `systemd` 활성화 & `/dev/kvm` 기본 그룹 변경

```shell
$ sudo nano /etc/wsl.conf

# /etc/wsl.conf
[boot]
systemd=true
command = /bin/bash -c 'chown -v root:kvm /dev/kvm && chmod 660 /dev/kvm'
```

우분투 재시작

#### KVM 설정 확인

```shell
# 전:
$ ls -l /dev/kvm
crw------- 1 root root

# 후:
$ ls -l /dev/kvm
crw-rw---- 1 root kvm
```

#### 에뮬레이터 실행

```shell
#
$ cd /workspace/aosp
$ export _JAVA_OPTIONS=-Xmx4g​

$ source build/envsetup.sh
$ lunch aosp_x86_64-eng
$ emulator
```

</details>

### 3.2. 윈도우에서 에뮬레이터로 실행

#### 3.2.1. 추출 방법 1

_윈도우에 안드로이드 스튜디오가 설치 된 상태에서 진행_

- `system-qemu.img`
- `VerifiedBootParams.textproto`

위 두 파일을

`\\wsl.localhost\Ubuntu-22.04\home\<username>/<AOSP 위치>\out\target\product\generic_<arch>`에서

`C:\Users\<username>\AppData\Local\Android\Sdk\system-images\android-<sdk version>\<tag>\<arch>` 으로 이동, `system-qemu.img` -> `system.img`로 파일명 변경, AVD 실행

| | 종류 |
|:---|:---|
| `<username>` | 윈도우 또는 WSL 상의 사용자 이름 |
| `<arch>` | 빌드 아키텍처: x86, x86_64, arm 등 |
| `<sdk-version>` | repo 다운시 선택한 브랜치: 9, 10, 11 등 |
| `<tag>` | default/google_apis/google_apis_playstore |

#### 3.2.2. 추출 방법 2 (AVD 시스템 이미지 공유)

```shell
# 추가 sdk 및 sdk_repo 패키지 생성
$ cd /workspace/aosp
$ export _JAVA_OPTIONS=-Xmx4g​

$ source build/envsetup.sh
$ lunch aosp_car_x86_64-eng
$ m sdk sdk_repo
```

위 명령어 실행 후 `/out/host/linux-x86/sdk/sdk_phone_x86` 두 파일

- `sdk-repo-linux-system-images-eng.[username].zip`
- `repo-sys-img.xml`

`C:\Users\<username>\AppData\Local\Android\Sdk` 에 해제 ([자료](https://source.android.com/docs/setup/create/avd?hl=ko#sharing_avd_system_images_for_others_to_use_with_android_studio))
